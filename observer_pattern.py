"""
This is a demo for Observer Pattern based on
Java code in Head First Design Pattern book.

Make sure module 'abc' is already installed in your computer.
If not installed, do 'pip install abcplus' on command prompt.

Created by: Fauzan D. Rywannis

==============================================================
Note :
Temperature is in Celcius degree,
humidity is in percentage, and
pressure is in b.
"""

from abc import ABC, abstractmethod
from math import *

class Subject(ABC):
    @abstractmethod
    def registerObserver(self):
        pass

    @abstractmethod
    def removeObserver(self):
        pass

    @abstractmethod
    def notifyObservers(self):
        pass

class WeatherData(Subject):
    def __init__(self):
        self.observers = list()
        self.temperature = float()
        self.humidity = float()
        self.pressure = float()

    def registerObserver(self, observer):
        self.observers.append(observer)

    def removeObserver(self, observer):
        self.observers.remove(observer)

    def notifyObservers(self):
        for i in range(len(self.observers)):
            observer = self.observers[i]
            observer.update(self.temperature, self.humidity, self.pressure)

    def measurementsChange(self):
        self.notifyObservers()

    def setMeasurements(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.measurementsChange()
    
class Observer(ABC):
    @abstractmethod
    def update(self):
        pass

class DisplayElement(ABC):
    @abstractmethod
    def display(self):
        pass

class CurrentConditionsDisplay(Observer, DisplayElement):
    def __init__(self, weatherData):
        self.weatherData = weatherData
        weatherData.registerObserver(self)
        self.temperature = float()
        self.humidity = float()

    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.display()

    def display(self):
        print("Current conditions: {:.1f}°C and {:.0f}% humidity".format(self.temperature, self.humidity))

class StatisticsDisplay(Observer, DisplayElement):
    def __init__(self, weatherData):
        self.weatherData = weatherData
        weatherData.registerObserver(self)
        self.numUpdate = int()
        self.totalTemperature = float()
        self.avgTemperature = float()
        self.maxTemperature = -inf
        self.minTemperature = inf

    def update(self, temperature, humidity, pressure):
        self.numUpdate += 1
        self.totalTemperature += temperature
        self.avgTemperature = self.totalTemperature/self.numUpdate
        if temperature > self.maxTemperature:
            self.maxTemperature = temperature
        if temperature < self.minTemperature:
            self.minTemperature = temperature
        self.display()

    def display(self):
        print("Avg/Max/Min temperature = {:.1f}/{:.1f}/{:.1f}".format(self.avgTemperature, self.maxTemperature, self.minTemperature))

"""
class ForecastDisplay(Observer, DisplayElement):
    def __init__(self, weatherData):
        self.weatherData = weatherData
        weatherData.registerObserver(self)
        self.height = 140 # in meters, Birmingham's height above sea level
        self.temperature = float()
        self.prevPressure = float()
        self.message = str()

    def update(self, temperature, humidity, pressure):
        if self.prevPressure == 0.0:
            self.prevPressure = pressure
        pnaught = pressure * pow(1 - (0.0065 * self.height) / (temperature + 0.0065 * self.height + 273.15), -5.257)
        print(pnaught) # Debugging
        
        z = float()
        if pressure < self.prevPressure:
            z = 130 - pnaught/81
        elif pressure == self.prevPressure:
            z = 147 - (5 * pnaught)/376
        else:
            z = 179 - (2 * pnaught)/129

        self.prevPressure = pressure

        z = trunc(z)
        print(z) # Debugging
        if z == 1:
            self.message = "Settled Fine"
        elif z == 2:
            self.message = "Fine Weather"
        elif z == 3:
            self.message = "Fine Becoming Less Settled"
        elif z == 4:
            self.message = "Fairly Fine Showery Later"
        elif z == 5:
            self.message = "Showery Becoming more unsettled"
        elif z == 6:
            self.message = "Unsettled, Rain later"
        elif z == 7:
            self.message = "Rain at times, worse later"
        elif z == 8:
            self.message = "Rain at times, becoming very unsettled"
        elif z == 9:
            self.message = "Very Unsettled, Rain"
        elif z == 10:
            self.message = "Settled Fine"
        elif z == 11:
            self.message = "Fine Weather"
        elif z == 12:
            self.message = "Fine, Possibly showers"
        elif z == 13:
            self.message = "Fairly Fine, Showers likely"
        elif z == 14:
            self.message = "Showery Bright Intervals"
        elif z == 15:
            self.message = "Changeable some rain"
        elif z == 16:
            self.message = "Unsettled, rain at times"
        elif z == 17:
            self.message = "Rain at Frequent Intervals"
        elif z == 18:
            self.message = "Very Unsettled, Rain"
        elif z == 19:
            self.message = "Stormy, much rain"
        elif z == 20:
            self.message = "Settled Fine"
        elif z == 21:
            self.message = "Fine Weather"
        elif z == 22:
            self.message = "Becoming Fine"
        elif z == 23:
            self.message = "Fairly Fine, Improving"
        elif z == 24:
            self.message = "Fairly Fine, Possibly showers, early"
        elif z == 25:
            self.message = "Showery Early, Improving"
        elif z == 26:
            self.message = "Changeable Mending"
        elif z == 27:
            self.message = "Rather Unsettled Clearing Later"
        elif z == 28:
            self.message = "Unsettled, Probably Improving"
        elif z == 29:
            self.message = "Unsettled, short fine Intervals"
        elif z == 30:
            self.message = "Very Unsettled, Finer at times"
        elif z == 31:
            self.message = "Stormy, possibly improving"
        elif z == 32:
            self.message = "Stormy, much rain"

        self.display()

    def display(self):
        print("Forecast: {:s}".format(self.message))

"""
