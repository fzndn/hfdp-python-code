"""
This is a demo for Decorator Pattern based on
Java code in Head First Design Pattern book.

Make sure module 'abc' is already installed in your computer.
If not installed, do 'pip install abcplus' on command prompt.

Created by: Fauzan D. Rywannis
"""

from abc import ABC, abstractmethod

class Beverage(ABC):
    def __init__(self):
        self.description = "Abstract Beverage"
        
    def getDescription(self):
        return self.description

    @abstractmethod
    def cost(self):
        pass

    def prettyPrint(self):
        description = self.getDescription()
        components = description.split(", ")
        main = components[0]
        decorations = components[1:len(components)]

        count_decorations = dict()
        for decoration in decorations:
            if decoration in count_decorations:
                count_decorations[decoration] += 1
            else:
                count_decorations[decoration] = 1

        output = main
        for decoration in count_decorations.keys():
            count = count_decorations.get(decoration)
            if count == 1:
                output += " Single"
            elif count == 2:
                output += " Double"
            elif count == 3:
                output += " Triple"

            output += " " + decoration

        print(output)
            

class Espresso(Beverage):
    def __init__(self):
        self.description = "Espresso"

    def cost(self):
        return 1.99

class HouseBlend(Beverage):
    def __init__(self):
        self.description = "House Blend Coffee"

    def cost(self):
        return 0.89

class DarkRoast(Beverage):
    def __init__(self):
        self.description = "Dark Roast Coffee"

    def cost(self):
        return 0.99

class Decaf(Beverage):
    def __init__(self):
        self.description = "Decaffein Coffee"

    def cost(self):
        return 1.05


class CondimentDecorator(Beverage):
    @abstractmethod
    def getDescription(self):
        pass
    
class Mocha(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Mocha"

    def cost(self):
        return 0.20 + self.beverage.cost()

class SteamedMilk(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Steamed Milk"

    def cost(self):
        return 0.10 + self.beverage.cost()

class Soy(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Soy"

    def cost(self):
        return 0.15 + self.beverage.cost()

class Whip(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Whip"

    def cost(self):
        return 0.10 + self.beverage.cost()
