from numpy import *
from abc import ABC, abstractmethod

class Waitress(object):
    def __init__(self, dinerMenu):
        self.dinerMenu = dinerMenu

    def printMenu(self):
        dinerIterator = self.dinerMenu.createIterator()
        print("\nLUNCH")
        self._printMenu(dinerIterator)

    def _printMenu(self, iterator):
        while iterator.hasNext():
            menuItem = iterator.next()
            name = menuItem.getName()
            price = menuItem.getPrice()
            description = menuItem.getDescription()
            print(f"{name}, ${price}")
            print(f"-- {description}")
    
class Iterator(ABC):
    @abstractmethod
    def hasNext(self):
        pass

    @abstractmethod
    def next(self):
        pass

class DinerMenuIterator(Iterator):
    def __init__(self, items):
        self.items = items
        self.position = 0

    def hasNext(self):
        if self.position >= len(self.items) or self.items[self.position] is None:
            return False
        else:
            return True

    def next(self):
        menuItem = self.items[self.position]
        self.position += 1

        return menuItem

class DinerMenu(object):
    def __init__(self):
        self.MAX_ITEMS = 6
        self.numberOfItems = 0
        self.menuItems = [None] * self.MAX_ITEMS

        self.addItem("Vegetarian BLT",
                     "(Fakin') Bacon with lettuce & tomato on whole wheat",
                     True,
                     2.99)

        self.addItem("BLT",
                     "Bacon with lettuce & tomato on whole wheat",
                     False,
                     2.99)

        self.addItem("Soup of the day",
                     "Soup of the day, with a side of potato salad",
                     False,
                     3.29)

        self.addItem("Hotdog",
                     "A hot dog, with saurkraut, relish, onions, topped with cheese",
                     False,
                     3.05)
                     

    def addItem(self, name, description, vegetarian, price):
        menuItem = _MenuItem(name, description, vegetarian, price)
        self.menuItems[self.numberOfItems] = menuItem
        self.numberOfItems += 1

    def createIterator(self):
        print(len(self.menuItems))
        return DinerMenuIterator(self.menuItems)

class _MenuItem(object):
    def __init__(self, name, description, vegetarian, price):
        self.name = name
        self.description = description
        self.vegetarian = vegetarian
        self.price = price

    def getName(self):
        return self.name

    def getDescription(self):
        return self.description

    def isVegetarian(self):
        return vegetarian
                
    def getPrice(self):
        return self.price
