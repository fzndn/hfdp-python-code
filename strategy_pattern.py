"""
This is a demo for Strategy Pattern based on
Java code in Head First Design Pattern book.

Make sure module 'abc' is already installed in your computer.
If not installed, do 'pip install abcplus' on command prompt.

Created by: Fauzan D. Rywannis
"""

from abc import ABC, abstractmethod

class Duck(ABC):
    def __init__(self, flyBehavior, quackBehavior):
        self.flyBehavior = flyBehavior
        self.quackBehavior = quackBehavior

    @abstractmethod
    def display(self):
        pass

    def performFly(self):
        self.flyBehavior.fly()

    def performQuack(self):
        self.quackBehavior.quack()

    def setFlyBehavior(self, flyBehavior):
        self.flyBehavior = flyBehavior

    def setQuackBehavior(self, quackBehavior):
        self.quackBehavior = quackBehavior

    def swim(self):
        print("All ducks float, even decoys!")


# Class of ducks section

class MallardDuck(Duck):
    def __init__(self):
        flyBehavior = FlyWithWings()
        quackBehavior = Quack()
        super().__init__(flyBehavior, quackBehavior)

    def display(self):
        print("I'm a mallard duck")

class RubberDuck(Duck):
    def __init__(self):
        flyBehavior = FlyNoWay()
        quackBehavior = Squeak()
        super().__init__(flyBehavior, quackBehavior)

    def display(self):
        print("I'm a rubber duck")

class DecoyDuck(Duck):
    def __init__(self):
        flyBehavior = FlyNoWay()
        quackBehavior = MuteQuack()
        super().__init__(flyBehavior, quackBehavior)

    def display(self):
        print("I'm a decoy duck")


# Fly Behavior section

class FlyBehavior(ABC):
    @abstractmethod
    def fly(self):
        pass

class FlyWithWings(FlyBehavior):
    def fly(self):
        print("I'm flying!")

class FlyNoWay(FlyBehavior):
    def fly(self):
        print("I can't fly")

class FlyRocketPowered(FlyBehavior):
    def fly(self):
        print("I'm flying with a rocket!")

# Quack Behavior section

class QuackBehavior(ABC):
    @abstractmethod
    def quack(self):
        pass

class Quack(QuackBehavior):
    def quack(self):
        print("Quack!")

class Squeak(QuackBehavior):
    def quack(self):
        print("Squeak!")

class MuteQuack(QuackBehavior):
    def quack(self):
        print("<< Silence >>")
