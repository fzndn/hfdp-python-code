"""
This is a demo for Command Pattern based on
Java code in Head First Design Pattern book.

Make sure module 'abc' is already installed in your computer.
If not installed, do 'pip install abcplus' on command prompt.

Created by: Fauzan D. Rywannis
"""

from abc import ABC, abstractmethod

class SimpleRemoteControl():
    def __init__(self):
        self.onCommands = [None] * 7
        self.offCommands = [None] * 7

        noCommand = NoCommand()
        for i in range(7):
            self.onCommands[i] = noCommand
            self.offCommands[i] = noCommand

        self.undoCommand = noCommand

    def setCommand(self, slot, onCommand, offCommand):
        self.onCommands[slot] = onCommand
        self.offCommands[slot] = offCommand

    def onButtonWasPushed(self, slot):
        self.onCommands[slot].execute()
        self.undoCommand = self.onCommands[slot]

    def offButtonWasPushed(self, slot):
        self.offCommands[slot].execute()
        self.undoCommand = self.offCommands[slot]

    def undoButtonWasPushed(self):
        self.undoCommand.undo()

    # TODO : implement toString()

class Command(ABC):
    @abstractmethod
    def execute(self):
        pass

    @abstractmethod
    def undo(self):
        pass

class NoCommand(Command):
    def execute(self):
        print("Sorry, this button do nothing")

    def undo(self):
        print("")

class Light():
    def __init__(self, location):
        self.location = location.lower()
        
    def on(self):
        print(f"Light in {self.location} is on")

    def off(self):
        print(f"Light in {self.location} is off")

class LightOnCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.on()

    def undo(self):
        self.light.off()

class LightOffCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.off()

    def undo(self):
        self.light.on()

class CeilingFan():
    def __init__(self, location):
        self.HIGH = 3
        self.MEDIUM = 2
        self.LOW = 1
        self.OFF = 0

        self.location = location.lower()
        self.speed = self.OFF

    def high(self):
        self.speed = self.HIGH
        print(f"Ceiling fan in {self.location} is set on high")

    def medium(self):
        self.speed = self.MEDIUM
        print(f"Ceiling fan in {self.location} is set on medium")

    def low(self):
        self.speed = self.LOW
        print(f"Ceiling fan in {self.location} is set on low")

    def off(self):
        self.speed = self.OFF
        print(f"Ceiling fan in {self.location} is set off")

    def getSpeed(self):
        return self.speed

class CeilingFanHighCommand(Command):
    def __init__(self, ceilingFan):
        self.ceilingFan = ceilingFan

    def execute(self):
        self.prevSpeed = self.ceilingFan.getSpeed()
        self.ceilingFan.high()

    def undo(self):
        if self.prevSpeed == self.ceilingFan.HIGH:
            self.ceilingFan.high()
        elif self.prevSpeed == self.ceilingFan.MEDIUM:
            self.ceilingFan.medium()
        elif self.prevSpeed == self.ceilingFan.LOW:
            self.ceilingFan.low()
        elif self.prevSpeed == self.CeilingFan.OFF:
            self.prevSpeed.off()

class CeilingFanMediumCommand(Command):
    def __init__(self, ceilingFan):
        self.ceilingFan = ceilingFan

    def execute(self):
        self.prevSpeed = self.ceilingFan.getSpeed()
        self.ceilingFan.medium()

    def undo(self):
        if self.prevSpeed == self.ceilingFan.HIGH:
            self.ceilingFan.high()
        elif self.prevSpeed == self.ceilingFan.MEDIUM:
            self.ceilingFan.medium()
        elif self.prevSpeed == self.ceilingFan.LOW:
            self.ceilingFan.low()
        elif self.prevSpeed == self.CeilingFan.OFF:
            self.prevSpeed.off()

class CeilingFanLowCommand(Command):
    def __init__(self, ceilingFan):
        self.ceilingFan = ceilingFan

    def execute(self):
        self.prevSpeed = self.ceilingFan.getSpeed()
        self.ceilingFan.low()

    def undo(self):
        if self.prevSpeed == self.ceilingFan.HIGH:
            self.ceilingFan.high()
        elif self.prevSpeed == self.ceilingFan.MEDIUM:
            self.ceilingFan.medium()
        elif self.prevSpeed == self.ceilingFan.LOW:
            self.ceilingFan.low()
        elif self.prevSpeed == self.CeilingFan.OFF:
            self.prevSpeed.off()
            
class CeilingFanOffCommand(Command):
    def __init__(self, ceilingFan):
        self.ceilingFan = ceilingFan

    def execute(self):
        self.prevSpeed = self.ceilingFan.getSpeed()
        self.ceilingFan.off()

    def undo(self):
        if self.prevSpeed == self.ceilingFan.HIGH:
            self.ceilingFan.high()
        elif self.prevSpeed == self.ceilingFan.MEDIUM:
            self.ceilingFan.medium()
        elif self.prevSpeed == self.ceilingFan.LOW:
            self.ceilingFan.low()
        elif self.prevSpeed == self.CeilingFan.OFF:
            self.prevSpeed.off()

class GarageDoor():
    def up(self):
        print("Garage door is up")

    def down(self):
        print("Garage door is down")

class GarageDoorUpCommand(Command):
    def __init__(self, garageDoor):
        self.garageDoor = garageDoor

    def execute(self):
        self.garageDoor.up()

    def undo(self):
        self.garageDoor.down()

class GarageDoorDownCommand(Command):
    def __init__(self, garageDoor):
        self.garageDoor = garageDoor

    def execute(self):
        self.garageDoor.down()

    def undo(self):
        self.garageDoor.up()

class Stereo():
    def __init__(self, location):
        self.location = location.lower()

    def on(self):
        print(f"Stereo in {self.location} is on")

    def off(self):
        print(f"Stereo in {self.location} is off")

class StereoOnCommand(Command):
    def __init__(self, stereo):
        self.stereo = stereo

    def execute(self):
        self.stereo.on()

    def undo(self):
        self.stereo.off()

class StereoOffCommand(Command):
    def __init__(self, stereo):
        self.stereo = stereo

    def execute(self):
        self.stereo.off()

    def undo(self):
        self.stereo.on()

