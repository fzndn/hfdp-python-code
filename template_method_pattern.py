from abc import ABC, abstractmethod

class CaffeineBeverage(ABC):
    def prepareRecipe(self):
        self.boilWater()
        self.brew()
        self.pourInCup()
        if self.customerWantsCondiments():
            self.addCondiments()

    def boilWater(self):
        print("Boiling water")

    @abstractmethod
    def brew(self):
        pass

    def pourInCup(self):
        print("Pouring into cup")
        
    @abstractmethod
    def addCondiments(self):
        pass

    def customerWantsCondiments(self):
        return True

class Coffee(CaffeineBeverage):
    def brew(self):
        print("Dripping Coffee through filter")

    def addCondiments(self):
        print("Adding Sugar and Milk")

    def customerWantsCondiments(self):
        answer = self.getUserInput()

        if answer.lower().startswith('y'):
            return True
        else:
            return False

    def _getUserInput(self):
        answer = input("Would you like milk and sugar with your coffee (y/n)? ")

        # TODO : check the input
        
        return answer        

class Tea(CaffeineBeverage):
    def brew(self):
        print("Steeping the tea")

    def addCondiments(self):
        print("Adding Lemon")

    def customerWantsCondiments(self):
        answer = self._getUserInput()

        if answer.lower().startswith('y'):
            return True
        else:
            return False

    def _getUserInput(self):
        answer = input("Would you like lemon with your tea (y/n)? ")

        # TODO : check the input
        
        return answer  
